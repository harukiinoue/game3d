using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KaihukuGene : MonoBehaviour
{
    public GameObject kaihuku;
    float span = 8f;
    float delte = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delte += Time.deltaTime;
        if (this.delte > this.span)
        {
            this.delte = 0;
            GameObject go = Instantiate(kaihuku) as GameObject;
            int px = Random.Range(-9, 9);
            go.transform.position = new Vector3(21, 1, px);
        }

    }
}
