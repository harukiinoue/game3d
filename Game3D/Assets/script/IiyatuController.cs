using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IiyatuController : MonoBehaviour
{
    public GameObject Ball;
    GameObject director;


    // Start is called before the first frame update
    void Start()
    {
        this.director = GameObject.Find("GameDirector");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.01f, 0, 0);//X方向に-0.01

        if (transform.position.x < -21.0f)
        {
            Destroy(gameObject);
        }

    }

    void OnTriggerEnter(Collider other)//何かにぶつかったとき
    {
        Destroy(Ball.gameObject);//ボールを消す
        this.director.GetComponent<GameDirector>().GetIiyatu();
    }

}
