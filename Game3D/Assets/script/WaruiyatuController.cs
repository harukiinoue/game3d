using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaruiyatuController : MonoBehaviour
{
    public GameObject Ball;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.02f, 0, 0);//X方向に-0.01

        if (transform.position.x < -21.0f)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)//何かとぶつかったとき
    {
        Destroy(Ball.gameObject);//ボールを消す

        GameObject director = GameObject.Find("GameDirector");
        director.GetComponent<GameDirector>().DecreaseHP();
    }

}
