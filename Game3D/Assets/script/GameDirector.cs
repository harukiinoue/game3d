using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    GameObject pointText;
    int point = 0;

    public void GetIiyatu()
    {
        this.point += 10;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge");
        this.pointText = GameObject.Find("point");
    }

    // Update is called once per frame
    void Update()
    {
        this.pointText.GetComponent<Text>().text = this.point.ToString() + "point";
    }

    public void DecreaseHP()
    {
        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;

        if(this.hpGauge.GetComponent<Image>().fillAmount==0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void RecoveryHP()
    {
        this.hpGauge.GetComponent<Image>().fillAmount += 0.1f;

    }
}
